package mb.gp.glasspyramid

import mb.math.assertFraction
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class GlassPyramidTest {

    @Test
    fun getGlass() {
        assertFraction(
            GlassPyramid.getGlass(43.toIndex(), 34.toIndex())!!.startsToSpillOverAt(),
            87960959120570,
            19683
        )
    }

}