package mb.math

import org.junit.jupiter.api.Assertions.assertEquals
import java.math.BigDecimal
import java.math.BigInteger

internal class FractionTest {

    @org.junit.jupiter.api.Test
    fun constructorReduction() {
        assertFraction(Fraction(0, 27), 0, 1)
        assertFraction(Fraction(-13, 26), -1, 2)
    }

    @org.junit.jupiter.api.Test
    fun reverse() {
        assertFraction(Fraction(77, 76).reverseOrZero(), 76, 77)
        assertFraction(Fraction(-1, 3).reverseOrZero(), -3)
        assertFraction(Fraction(0, 5).reverseOrZero(), 0)
    }

    @org.junit.jupiter.api.Test
    fun toBigDecimal() {
    }

    @org.junit.jupiter.api.Test
    fun compareTo() {
        assert(Fraction(1, 1).compareTo(Fraction(2, 2)) == 0)
    }

    @org.junit.jupiter.api.Test
    fun minus() {
        assertFraction(Fraction.ONE - Fraction.TEN, -9)
        assertFraction(Fraction(12, 13) - Fraction(7, 6), -19, 78)
    }

    @org.junit.jupiter.api.Test
    fun plus() {
    }

    @org.junit.jupiter.api.Test
    fun times() {
    }

    @org.junit.jupiter.api.Test
    fun div() {
    }

    @org.junit.jupiter.api.Test
    fun copy() {
    }

    @org.junit.jupiter.api.Test
    fun testToString() {
    }

    @org.junit.jupiter.api.Test
    fun testHashCode() {
    }

    @org.junit.jupiter.api.Test
    fun testEquals() {
        assert(Fraction(1, 1) == Fraction(2, 2))
    }
}