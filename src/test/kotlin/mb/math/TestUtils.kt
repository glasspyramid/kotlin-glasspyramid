package mb.math

import org.junit.jupiter.api.Assertions
import java.math.BigInteger

fun assertFraction(value: Fraction, numerator: Long, denominator: Long = 1)
{
    Assertions.assertEquals(BigInteger.valueOf(numerator), value.numerator)
    Assertions.assertEquals(BigInteger.valueOf(denominator), value.denominator)
}
