package mb.gp.glasspyramid

import mb.gp.glasspyramid.GlassPyramid.isValidGlassId

class GlassRow(private val rowIndex: UShort) {

    private val glasses: MutableMap<Index, Glass> = mutableMapOf()

    fun getGlass(glassIndex: Index) =
        if (isValidGlassIndex(glassIndex))
            glasses.getOrPut(glassIndex) { Glass(rowIndex, glassIndex) }
        else
            null

    private fun isValidGlassIndex(glassIndex: Index) = isValidGlassId(GlassId(rowIndex, glassIndex))
}
