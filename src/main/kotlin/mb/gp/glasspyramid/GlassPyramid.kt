package mb.gp.glasspyramid

object GlassPyramid {
    private const val MAX_ROW = 500u
    private val glassRows: MutableMap<Index, GlassRow> = mutableMapOf()

    operator fun invoke(glassId: GlassId) = getGlass(glassId)

    fun getGlass(glassId: GlassId) = getGlass(glassId.rowIndex, glassId.glassIndex)

    fun getGlass(rowIndex: Index, glassIndex: Index): Glass? =
        if (isValidRowIndex(rowIndex))
            glassRows.getOrPut(rowIndex) { GlassRow(rowIndex) }.getGlass(glassIndex)
        else
            null

    fun validateGlassId(glassId: GlassId): Result<Unit> =
        when {
            !isValidRowIndex(glassId.rowIndex) -> Result.failure(RuntimeException("1 <= row <= 500"))
            !isValidGlassId(glassId) -> Result.failure(RuntimeException("1 <= glass <= row"))
            else -> Result.success(Unit);
        }

    fun isValidGlassId(glassId: GlassId) =
        1.toIndex() <= glassId.glassIndex && glassId.glassIndex <= glassId.rowIndex

    fun isValidRowIndex(index: Index) = index in 1u..MAX_ROW
}