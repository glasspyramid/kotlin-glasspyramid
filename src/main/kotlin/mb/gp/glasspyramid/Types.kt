package mb.gp.glasspyramid

typealias Index = UShort

fun String.toIndex(): Index? = toUShortOrNull()

fun UInt.toIndex(): Index = toUShort()

fun Int.toIndex(): Index = toUShort()

data class GlassId(val rowIndex: Index, val glassIndex: Index)
