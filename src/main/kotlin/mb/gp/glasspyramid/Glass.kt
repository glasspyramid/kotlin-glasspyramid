package mb.gp.glasspyramid

import mb.math.Fraction

class Glass(private val rowIndex: Index, private val glassIndex: Index) {

    // The pyramid starts to fill up att time 0
    private val initialPointInTime = Fraction.ZERO

    // The top glass fills up with the speed 10 time-units / glass
    private val topGlassFillSpeed = Fraction.TEN

    private val deliverySpeed: SpeedChange = calculateDeliverySpeed()

    fun startsToSpillOverAt() = deliverySpeed.time // prop with delegation ??

    private fun calculateDeliverySpeed(): SpeedChange {
        val parentDeliverySpeeds = calculateParentDeliverySpeeds()
        val firstSpeedChange = parentDeliverySpeeds.firstSpeedChange
        val secondSpeedChange = parentDeliverySpeeds.secondSpeedChange

        var glassFillLevel: Fraction = Fraction.ZERO
        var glassWillFillUpAt: Fraction = calculatePointInTimeWhenGlassWillFillUp(glassFillLevel, firstSpeedChange)

        if (secondSpeedChange == null || secondSpeedChange.time > glassWillFillUpAt) {
            return SpeedChange(glassWillFillUpAt, firstSpeedChange.fillSpeedToDeliverySpeed())
        }

        glassFillLevel = increaseGlassFillLevel(glassFillLevel, firstSpeedChange, secondSpeedChange)
        glassWillFillUpAt = calculatePointInTimeWhenGlassWillFillUp(glassFillLevel, secondSpeedChange)

        return SpeedChange(glassWillFillUpAt, secondSpeedChange.fillSpeedToDeliverySpeed())
    }

    private fun calculateParentDeliverySpeeds(): SpeedChangeSequence {
        val previousRow = (rowIndex - 1.toIndex()).toIndex()
        val previousGlass = (glassIndex - 1.toIndex()).toIndex()

        val parentLeft = GlassPyramid.getGlass(previousRow, previousGlass)
        val parentRight = GlassPyramid.getGlass(previousRow, glassIndex)

        return when {
            parentLeft != null && parentRight != null ->
                SpeedChangeSequence(parentLeft.deliverySpeed, parentRight.deliverySpeed)
            parentLeft != null ->
                SpeedChangeSequence(parentLeft.deliverySpeed)
            parentRight != null ->
                SpeedChangeSequence(parentRight.deliverySpeed)
            else ->
                SpeedChangeSequence(
                    // A glass without parents is the top most glass in the pyramid
                    SpeedChange(initialPointInTime, topGlassFillSpeed)
                )
        }
    }

    private fun calculatePointInTimeWhenGlassWillFillUp(
        glassFillLevel: Fraction,
        fillSpeed: SpeedChange
    ): Fraction {
        val remainsToFillUp = Fraction.ONE - glassFillLevel
        val glassWillFillUpIn = fillSpeed.speed * remainsToFillUp
        return fillSpeed.time + glassWillFillUpIn
    }

    private fun increaseGlassFillLevel(
        currentFillLevel: Fraction,
        currentSpeed: SpeedChange,
        nextSpeed: SpeedChange
    ): Fraction {
        val timeReceivingAtCurrentSpeed = nextSpeed.time - currentSpeed.time
        val volumeReceivedAtCurrentSpeed = timeReceivingAtCurrentSpeed / currentSpeed.speed
        return currentFillLevel + volumeReceivedAtCurrentSpeed
    }

}