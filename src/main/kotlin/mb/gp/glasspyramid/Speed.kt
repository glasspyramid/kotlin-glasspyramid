package mb.gp.glasspyramid

import mb.math.Fraction

// Speed is expressed as time/glass.
// Fill speed is the speed with which a glass i filling up. The top glass fills up with 10/1.
// Delivery speed is the speed with which a glass delivers to one of its two child glasses.
// The top glass hence delivers at the speed 20/1.

fun combineSpeeds(speed1: Fraction, speed2: Fraction) =
    (speed1.reverseOrZero() + speed2.reverseOrZero()).reverseOrZero()

fun fillSpeedToDeliverySpeed(fillSpeed: Fraction) =
    fillSpeed * (Fraction.TWO)

class SpeedChange(val time: Fraction, val speed: Fraction) {

    fun addSpeed(additionalSpeed: Fraction) =
        SpeedChange(time, combineSpeeds(speed, additionalSpeed))

    fun fillSpeedToDeliverySpeed() = fillSpeedToDeliverySpeed(speed)
}

class SpeedChangeSequence(speedChangeA: SpeedChange, speedChangeB: SpeedChange? = null) {
    val firstSpeedChange: SpeedChange
    val secondSpeedChange: SpeedChange?

    init {
        if (speedChangeB == null) {
            firstSpeedChange = speedChangeA
            secondSpeedChange = null
        } else
            when {
                speedChangeA.time < speedChangeB.time -> {
                    firstSpeedChange = speedChangeA
                    secondSpeedChange = speedChangeB.addSpeed(speedChangeA.speed)
                }
                speedChangeA.time > speedChangeB.time -> {
                    firstSpeedChange = speedChangeB
                    secondSpeedChange = speedChangeA.addSpeed(speedChangeB.speed)
                }
                else -> {
                    firstSpeedChange = speedChangeA.addSpeed(speedChangeB.speed)
                    secondSpeedChange = null
                }
            }
    }
}
