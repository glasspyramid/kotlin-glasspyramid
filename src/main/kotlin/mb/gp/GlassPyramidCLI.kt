package mb.gp

import mb.gp.glasspyramid.GlassId
import mb.gp.glasspyramid.GlassPyramid
import mb.gp.glasspyramid.Index
import mb.gp.glasspyramid.toIndex
import mb.math.Fraction

fun main(args: Array<String>) = run()

private const val QUIT_CODE = "q"
private const val FRACTIONAL_CODE = "f"
private const val DECIMAL_CODE = "d"

private var fractionalMode = false

fun run() {
    writeIntro()

    do {
        val glassId: GlassId = readRequiredGlass() ?: return

        with(GlassPyramid.validateGlassId(glassId)) {
            if (isSuccess)
                writeSpillOverTime(GlassPyramid.getGlass(glassId)!!.startsToSpillOverAt())
            else
                println(exceptionOrNull()?.message ?: "Unexpected null value")
        }
    } while (true)
}

private fun writeIntro() {
    println()
    println("* The glass pyramid *")
    println()
    println("$DECIMAL_CODE = Decimal display")
    println("$FRACTIONAL_CODE = Fractional display")
    println("$QUIT_CODE = Quit")
    println()
}

private fun setAndConfirmFractionalMode(fractionalModeOn: Boolean) {
    fractionalMode = fractionalModeOn
    when (fractionalMode) {
        true -> println("Fractional display selected")
        false -> println("Decimal display selected")
    }
}

private fun writeSpillOverTime(startsToSpillOverAt: Fraction) {
    val spillOverTime = if (fractionalMode)
        startsToSpillOverAt
    else
        startsToSpillOverAt.toBigDecimal(3)
    println("It takes $spillOverTime seconds for the glass to fill up")
    println("")
}

private fun readRequiredGlass() =
    promptForIndexValue("Row ? ")?.let { rowIndex ->
        promptForIndexValue("Glass ? ")?.let { glassIndex ->
            GlassId(rowIndex, glassIndex)
        }
    }

private fun promptForIndexValue(prompt: String): Index? {
    do {
        print(prompt)

        val input = readLine()?.trim()

        if (!input.isNullOrEmpty())
            when {
                input.equals(QUIT_CODE, true) ->
                    return null
                input.equals(FRACTIONAL_CODE, true) ->
                    setAndConfirmFractionalMode(true)
                input.equals(DECIMAL_CODE, true) ->
                    setAndConfirmFractionalMode(false)
                else ->
                    input.toIndex()?.let { return it }
            }
    } while (true)
}

