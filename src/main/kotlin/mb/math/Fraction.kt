package mb.math

import java.math.BigDecimal
import java.math.BigInteger
import java.math.RoundingMode

// TODO: Implement Number
class Fraction(numerator: BigInteger, denominator: BigInteger) : Comparable<Fraction> {

    companion object {
        val ZERO = Fraction(BigInteger.ZERO, BigInteger.ONE)
        val ONE = Fraction(BigInteger.ONE, BigInteger.ONE)
        val TWO = Fraction(BigInteger.valueOf(2), BigInteger.ONE) // BigInteger.TWO is private in java 8
        val TEN = Fraction(BigInteger.TEN, BigInteger.ONE)
    }

    val numerator: BigInteger
    val denominator: BigInteger

    init {
        if (denominator <= BigInteger.ZERO)
            throw IllegalArgumentException("Denominator must be greater than zero")

        val commonFactor = findGreatestCommonDivisor(numerator, denominator)

        this.numerator = numerator / commonFactor
        this.denominator = denominator / commonFactor
    }

    constructor(value: BigInteger) : this(value, BigInteger.ONE)

    constructor(value: Number) : this(BigInteger.valueOf(value.toLong()), BigInteger.ONE)

    constructor(numerator: Long, denominator: Long) :
            this(BigInteger.valueOf(numerator), BigInteger.valueOf(denominator))

    private fun findCommonDenominator(denominatorA: BigInteger, denominatorB: BigInteger) =
        (denominatorA * denominatorB) / findGreatestCommonDivisor(denominatorA, denominatorB)

    private fun findGreatestCommonDivisor(argA: BigInteger, argB: BigInteger): BigInteger =
        if (argA == BigInteger.ZERO)
            argB.abs()
        else
            findGreatestCommonDivisor(argB % argA, argA)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Fraction
        return (numerator == other.numerator) && (denominator == other.denominator)
    }

    override fun hashCode() = 31 * numerator.hashCode() + denominator.hashCode()

    override fun compareTo(other: Fraction) =
        ((numerator * other.denominator) - (denominator * other.numerator)).compareTo(BigInteger.ZERO)

    override fun toString(): String = "${numerator.toString()}/${denominator.toString()}"

    fun reverse() =
        when {
            numerator > BigInteger.ZERO -> Fraction(denominator, numerator)
            numerator < BigInteger.ZERO -> Fraction(-denominator, -numerator)
            else -> null
        }

    fun reverseOrZero() = reverse() ?: ZERO

    fun toBigDecimal(scale: Int, roundingMode: RoundingMode = RoundingMode.HALF_EVEN): BigDecimal =
        BigDecimal(numerator).divide(BigDecimal(denominator), scale, roundingMode)

    operator fun minus(subtrahend: Fraction): Fraction {
        val newDenominator = findCommonDenominator(denominator, subtrahend.denominator)
        var newNumerator = numerator * (newDenominator / denominator)
        newNumerator -= (subtrahend.numerator * (newDenominator / (subtrahend.denominator)))
        return Fraction(newNumerator, newDenominator)
    }

    operator fun plus(addend: Fraction): Fraction {
        val newDenominator = findCommonDenominator(denominator, addend.denominator)
        var newNumerator = numerator * (newDenominator / denominator)
        newNumerator += addend.numerator * (newDenominator / addend.denominator)
        return Fraction(newNumerator, newDenominator)
    }

    operator fun times(multiplier: Fraction): Fraction {
        val newNumerator = numerator * multiplier.numerator
        val newDenominator = denominator * multiplier.denominator
        return Fraction(newNumerator, newDenominator)
    }

    operator fun div(divisor: Fraction): Fraction {
        val newNumerator = numerator * divisor.denominator
        val newDenominator = denominator * divisor.numerator.abs()
        return Fraction(newNumerator, newDenominator)
    }

}